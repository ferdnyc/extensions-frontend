#!/bin/bash

NGINX_CONFIGURATION=/etc/nginx/conf.d/default.conf
test -z ${NGINX_REAL_IP_FROM+x} && NGINX_REAL_IP_FROM="10.0.0.0/8 172.16.0.0/12 192.168.0.0/16"

cat <<EOF > "${NGINX_CONFIGURATION}"
server {
    listen    8080 default_server;
    listen    [::]:8080 default_server;

    charset utf-8;

    sendfile                        on;
    tcp_nopush                      on;
    tcp_nodelay                     on;
    server_tokens                   off;
    client_max_body_size            ${NGINX_CLIENT_MAX_BODY_SIZE:-1m};

    keepalive_timeout  65;

    gzip on;
    gzip_types text/css text/html text/plain application/javascript application/json application/xml;

    location / {
        try_files \$uri \$uri/ /index.html;
    }

    location /healthz {
      access_log off;
      return 200;
    }

    root /usr/share/nginx/html;
EOF

for ip in ${NGINX_REAL_IP_FROM:-10.0.0.0/8 172.16.0.0/12 192.168.0.0/16}; do
	echo "    set_real_ip_from $ip;" >> "${NGINX_CONFIGURATION}"
done

echo "}" >> "${NGINX_CONFIGURATION}"

CONFIG_FILE="/usr/share/nginx/html/config.json"
CONFIG_SUFFIX="EGO_JSON_CONFIG_"
CONFIG_VARS="$(declare -p | awk -F '[ =]' "\$3 ~ /^${CONFIG_SUFFIX}/ { print \$3 }")"

if test -n "${CONFIG_VARS}"; then
    jq_args=( )
    jq_query='.'
    CONFIG_VARS=($CONFIG_VARS)

    for index in "${!CONFIG_VARS[@]}"; do
        key="${CONFIG_VARS[$index]}"
        value="${!key}"

        key="${key#"$CONFIG_SUFFIX"}"

        if test -n "${key}"; then
            jq_args+=( --arg "key$index"   "${key}"   )
            jq_args+=( --arg "value$index" "${value}" )
            jq_query+=" | .[\$key${index}]=\$value${index}"
        fi
    done

    jq "${jq_args[@]}" "$jq_query" <<<'{}' > "${CONFIG_FILE}"
fi
